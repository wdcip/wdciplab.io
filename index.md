---
layout: default
title: Call for Participation
---

## Call for Participation
### June 23, 2019 in San Diego, CA

### Update (Merge)

We are merging our workshop with W4: [Towards a Research Agenda for Gameful Creativity](https://gamefulcreativity18.wordpress.com/). 

The merged workshop aims to bring together both researchers and practitioners interested in how play and gamefulness enhance and support creative thinking. Play and creativity are intertwined. Play involves imagination and curiosity wherein players explore potential actions and realities. In collaborative and social play, creative contributions are distributed among players. Correspondingly, gameful design can give rise to playful behaviors. To that end, we argue that gamefulness and games are approaches to activate, explore, and promote creativity.  To promote an engaging exchange of experience, insights and ideas as well as help build a research agenda, the workshop will include two participatory, hands-on activities: a gameful ideation session to identify potential for combining gamefulness, play, and creativity; and a video prototyping session to design and explore aspects of usability and experience for those potentials identified in the ideation session.

We will continue to accept submissions from the original calls for both workshops. Accepted submissions will be presented by the authors in an introductory session that will frame the agenda and topics of this merged workshop.

### Original Call (W1: Distributed Creativity in Play)

We invite submissions from researchers, game designers, players, and others to participate in a one-day workshop on Distributed Creativity in Play at [ACM Creativity & Cognition 2019](http://cc.acm.org/2019/) in San Diego.

Play and creativity are intertwined. Play involves imagination and curiosity wherein players explore potential actions and realities. In collaborative and social play, creative contributions are distributed among players. For example, in tabletop role-playing games, such as [Dungeons & Dragons](http://dnd.wizards.com), each player verbalizes actions of their character(s), contributing to the creative generation of a shared narrative. [Sawyer and DeZutter](https://pdfs.semanticscholar.org/bd5e/034dfae4054552bce7f0c43b60514370a750.pdf) identify this phenomenon as _distributed creativity_, "where individuals within groups contribute ideas often towards a common goal."  

Distributed creativity is not limited to groups in the same physical space. Increasingly popular web-based platforms, such as  [Twitch](https://www.twitch.tv) and [Roll20](https://roll20.net), connect participants from all over the world in shared play experiences. They offer opportunities to design for and study distributed creativity in digitally mediated and situated contexts. However, we suspect that qualities of these online spaces, their designs, and associated technologies affect distributed creativity. For example, these spaces can promote creative contributions of participants through designs that increase agency or limit involvement through technological constraints. [Twitch's Clips feature](https://help.twitch.tv/s/article/how-to-use-clips) provides one such example, enabling participants to creatively curate interesting play moments during a live stream. Clips are automatically shared in the stream's text chat, allowing participants to contribute to shared play experience. 

This workshop will explore different contexts, study methods, game designs, and technologies associated with distributed creativity in play. Invited participants will present their work during a show-and-tell session. They will video prototype new play experiences involving distributed creativity, engage in critical reflections on related topics, and contribute to workshop outcomes.

#### Objectives
The primary objectives of this workshop are to:

*  identify characteristics of distributed creativity in play;
*  critically reflect on how design and technology draw upon, enable, limit, or negate these characteristics;
*  share theories, methodologies, practices, and environments for supporting and studying distributed creativity in play;
*  foster future collaborations on this topic.

#### Topics
Topics of interest include but are not limited to:

* play contexts involving distributed creativity, including:
    * tabletop games (in-person and online)
    * tabletop role-playing games (in-person and online)
    * live action role-playing
    * multiplayer video games (in-person and online)
    * game play live streaming (e.g., [Twitch](https://www.twitch.tv))
* methods for studying distributed creativity (in play), including:
    * quantitative, qualitative, and mixed-methods approaches
    * process-oriented methods
    * research through design
* effects of play, technology, or game design on distributed creativity

#### Submissions

To participate in this workshop, please send your position paper to [dcipworkshop@googlegroups.com](mailto:dcipworkshop@googlegroups.com?subject=Position%20Paper%20Submission%20for%20DIS%20Workshop%20on%20Distributed%20Creativity%20in%20Play). Position papers should be PDF document and have a maximum length of 1500 words (excluding title, authors, abstract, and references). 


#### Important Dates

{% include dates.md %}



