---
name: Ross Graeber
order: 6
portrait: /images/RGraeber.jpg
---

Ross is a Senior Software Engineer working for Schlumberger Limited.  He focuses on User Experience design for service applications in energy exploration and production. Ross has interest in how social communities influence the development of games and how collaboration outside the game itself can effect play. He currently leads and plays multiple online versions of traditionally tabletop games.