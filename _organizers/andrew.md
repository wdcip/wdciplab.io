---
name: Andrew Webb
order: 1
portrait: /images/andrew2.png
affiliation: Centrum Wiskunde & Informatica (CWI)
website: http://andrewmwebb.com
---

Andrew is an ERCIM Postdoctoral Fellow at CWI in the Distributed and Interactive Systems group. A central theme of his research is how new interactive environments can support creativity. He is presently investigating the design of seamful media spaces for distributed tabletop role-playing games. He is actively involved in the Creativity & Cognition conference, serving on the program committee, co-chairing posters and demos, and publishing papers.