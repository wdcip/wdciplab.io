---
name: Wendy Mackay
order: 7
portrait: /images/wendy.jpg
affiliation: Inria, Univ. Paris-Sud, CNRS, Université Paris-Saclay
website: https://ex-situ.lri.fr/people/mackay/
---

Wendy is a Research Director, Classe Exceptionnelle, at Inria, France, where she heads the ExSitu (Extreme Situated Interaction) research group in Human-Computer Interaction at the Université Paris-Saclay. After receiving her Ph.D. from MIT, she managed research groups at Digital Equipment and Xerox EuroPARC, which were among the first to explore interactive video and tangible computing. She has been a visiting professor at University of Aarhus and Stanford University and recently served as Vice President for Research at the University of Paris-Sud. Wendy is a member of the ACM CHI academy, is a past chair of ACM/SIGCHI, chaired CHI’13, and the recipient of the ACM/SIGCHI Lifetime Acheivement Service Award and a Doctor Honoris Causa from Aarhus University. She received an ERC Advanced Grant for her research on human-computer partnerships. She has published over 200 peer-reviewed research articles in the area of Human-computer Interaction. Her current research interests include the design of interactive tools, using human-computer partnerships, to support creativity, as well as participatory and generative research and design methods.