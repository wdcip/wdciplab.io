---
name: Nic Lupfer
order: 5
affiliation: Interface Ecology Lab, Texas A&M University
---

Nic is a doctoral candidate at Texas A&M University.
His doctoral research investigates new ways for supporting collaborative design ideation among student teams.
Nic is also an avid participant in multiple local and global game jams and serves as a mentor in the local student International Game Developers Association chapter.